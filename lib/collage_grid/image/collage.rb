module CollageGrid
  module Image
    class Collage
      attr_accessor :images, :path

      def initialize(filename, images)
        @path = "./tmp/#{validate_or_add_extension(filename)}"
        @images = images
      end

      def perform
        montage = MiniMagick::Tool::Montage.new
        images.map{|image| montage << image }
        opts = ['-auto-orient', '-bordercolor', 'Lavender', '-gravity', 'center', '+polaroid']
        opts.map{|opt| montage << opt}
        montage << path
        montage.call
      end

      private
      def validate_or_add_extension(filename)
        filename.gsub!(" ", "_")
        ext = File.extname(filename)
        valid_ext?(ext) ? filename : "#{filename}.jpg"
      end

      def valid_ext?(ext)
        !ext.empty? && ext.match(/jpg|jpeg|png/)
      end
    end
  end
end
