module CollageGrid
  module Errors
    module Flickr
      class NotFound < StandardError
      end
    end
  end
end
