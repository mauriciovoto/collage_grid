module CollageGrid
  module Services
    module Processor
      module_function

      def perform(words, collage_filename)
        images = words.flatten.map do |word|
          result = flickr_search(Flickr::Searcher.new, word.strip)
          Flickr::Photo.new(result).download_image
        end

        collage = Image::Collage.new(collage_filename, images)
        collage.perform
        puts collage.path
      end

      def flickr_search(searcher, word)
        begin
          searcher.search(word)
        rescue
          word = CollageGrid.dictionary.random_words
          retry
        end
      end
    end
  end
end
