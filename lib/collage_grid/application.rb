module CollageGrid
  class Application
    def initialize(words)
      File.delete(*Dir.glob('./tmp/*.jpg'))
      # First ten is used here to avoid more than 10 words as input + the
      # ouput filename that it's the last arg
      @collage_filename = words.pop.split("/").last
      @words = words.first(10)
      @dictionary = CollageGrid.dictionary
    end

    def run
      fetch_random_words(words) if words.size < 10

      Services::Processor.perform(words, collage_filename)
    end

    private
    attr_reader :collage_filename, :words

    def fetch_random_words(words)
      missing_words = 10 - words.size
      words << words_from_dictionary(missing_words)
    end

    def words_from_dictionary(number_of_words)
      @dictionary.random_words(number_of_words)
    end
  end
end
