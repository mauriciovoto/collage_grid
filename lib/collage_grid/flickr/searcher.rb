module CollageGrid
  module Flickr
    class Searcher
      def initialize
        CollageGrid.flickr_auth
        @search_args = basic_search_args
      end

      def search(word)
        search_args[:text] = word
        result = flickr.photos.search(search_args)
        raise Errors::Flickr::NotFound if result.nil? || result.to_a.empty?
        result
      end

      private
      attr_reader :search_args

      # More information on:
      # https://www.flickr.com/services/api/flickr.photos.search.html
      def basic_search_args
        {
          accuracy: 1,
          page: 1,
          per_page: 1,
          media: 'photos',
          sort: 'interestingness-desc'
        }
      end
    end
  end
end
