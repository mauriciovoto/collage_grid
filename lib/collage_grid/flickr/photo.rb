module CollageGrid
  module Flickr
    class Photo
      attr_reader :image

      # search_result is the response from the Flickr API term search
      def initialize(search_result)
        CollageGrid.flickr_auth
        id, secret = search_result[0].id, search_result[0].secret
        info = flickr.photos.getInfo(photo_id: id, secret: secret)
        @url = FlickRaw.url_n(info)
        @file_path = to_jpg("./tmp/#{url.split('/').last}")
      end

      def download_image
        img = MiniMagick::Image.open(url)
        img.write(file_path)

        @image = MiniMagick::Image.open(file_path)
        crop
        file_path
      end

      private
      attr_reader :url, :file_path

      def to_jpg(file_path)
        file_path.gsub(File.extname(file_path), ".jpg")
      end

      # Crops retangularly, by removing 40% from image's y axis dimension
      def crop
        x,y = image.dimensions
        image.crop("#{x}x#{y-(y*0.4)}+0+0") if x == y
        image.write file_path
      end
    end
  end
end
