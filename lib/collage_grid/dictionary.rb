module CollageGrid
  class Dictionary
    def initialize
      @dictionary = File.readlines('./doc/dictionary')
    end

    def random_words(number=1)
      @dictionary.sample(number)
    end

    private
    attr_reader :dictionary
  end
end
