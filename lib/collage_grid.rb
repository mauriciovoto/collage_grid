Bundler.require :default
require 'pry' unless ENV['RUBY_ENV'] == 'production'

module CollageGrid
  module_function

  def flickr_auth
    config = YAML::load_file(File.join('./config', 'flickr.yml'))

    FlickRaw.api_key       = config['key']
    FlickRaw.shared_secret = config['secret']
  end

  def dictionary
    @dictionary ||= Dictionary.new
  end
end

require './lib/collage_grid/dictionary'
require './lib/collage_grid/errors/flickr/not_found'
require './lib/collage_grid/flickr/searcher'
require './lib/collage_grid/flickr/photo'
require './lib/collage_grid/image/collage'
require './lib/collage_grid/services/processor'
require './lib/collage_grid/application'
