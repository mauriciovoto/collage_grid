require 'simplecov'

SimpleCov.start do
  refuse_coverage_drop
  add_filter '/spec/'
end

require 'rspec'
require 'pry'
require 'vcr'
require './lib/collage_grid'

VCR.configure do |config|
  config.cassette_library_dir = "spec/vcr_cassettes"
  config.hook_into :webmock
  config.configure_rspec_metadata!
end

RSpec.configure do |config|
  config.before(:each) do
    File.delete(*Dir.glob('./tmp/*.jpg'))
  end
end
