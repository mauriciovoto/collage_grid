require 'spec_helper'

describe CollageGrid::Dictionary do
  describe '#random_words' do
    let(:dictionary) { ['ball', 'sun', 'dark', 'river', 'shadow', 'war'] }
    before { allow(File).to receive(:readlines).and_return(dictionary) }

    it 'returns a random word from the dictionary' do
      expect(dictionary).to include(described_class.new.random_words.join)
    end

    context 'without params' do
      it 'returns one word' do
        expect(described_class.new.random_words.size).to eq(1)
      end
    end

    context 'given 3 as param' do
      it 'returns 3 words' do
        expect(described_class.new.random_words(3).size).to eq(3)
      end
    end
  end
end
