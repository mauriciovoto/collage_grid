require 'spec_helper'

describe CollageGrid::Application do
  describe '.run' do
    context 'given less than 10 words and one output string',
      vcr: {cassette_name: 'application/random_words'} do
      let(:words) { ['sky', 'sun', 'moon', 'pen', 'workout', 'user output'] }
      let(:application) { described_class.new(words) }
      before do
        allow(application).to receive(:words_from_dictionary).with(5).
          and_return('soccer', 'woman', 'balloon', 'party', 'cake', 'notebook')
      end

      it 'downloads images 10 times' do
        expect(application).to receive(:words_from_dictionary).with(5)
        application.run
      end
    end

    context 'given 10 words and one output string',
      vcr: {cassette_name: 'application/10_words'} do
      let(:words) do
        [
          'sky', 'sun', 'moon', 'dog', 'baby', 'cat', 'gun', 'wall', 'sea',
          'pen', 'user output'
        ]
      end

      it 'downloads images 10 times' do
        expect(described_class).to_not receive(:words_from_dictionary)
        described_class.new(words).run
      end
    end
  end
end
