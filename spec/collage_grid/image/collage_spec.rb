require 'spec_helper'

describe CollageGrid::Image::Collage do
  describe '#perform' do
    context 'given jpg files on the tmp folder' do
      let!(:collage) do
        described_class.new('output', ['./tmp/panda.jpg', './tmp/baby_dota.jpg'])
      end

      before do
        FileUtils.cp('./spec/fixtures/panda.jpg', './tmp/panda.jpg')
        FileUtils.cp('./spec/fixtures/baby_dota.jpg', './tmp/baby_dota.jpg')
      end

      it 'gets all files and mounts the collage file' do
        collage.perform
        expect(Dir.entries('./tmp')).to include('output.jpg')
      end
    end
  end
end
