require 'spec_helper'

describe CollageGrid::Services::Processor do
  describe '.perform', vcr: {cassette_name: 'application/10_words'} do
    let(:words) do
      ['sky', 'sun', 'moon', 'dog', 'baby', 'cat', 'gun', 'wall', 'sea', 'pen']
    end

    it 'performs the collage' do
      expect(STDOUT).to receive(:puts).with('./tmp/user_output.jpg')
      described_class.perform(words, 'user output')
    end
  end

  describe '.flickr_search' do
    context 'given term with results(no retry)',
      vcr: {cassette_name: 'processor/with_results'} do
      let(:searcher) { CollageGrid::Flickr::Searcher.new }
      let(:term_with_result) { 'linux' }

      it 'searches once' do
        expect_any_instance_of(CollageGrid::Dictionary).to_not receive(
          :random_words
        )
        described_class.flickr_search(searcher, term_with_result)
      end
    end

    context 'given term without results(retry)',
      vcr: {cassette_name: 'processor/without_first_results'} do
      let(:searcher) { CollageGrid::Flickr::Searcher.new }
      let(:term_without_result) { 'jkdfuiejksdowqosa' }

      it 'searches at least twice' do
        expect_any_instance_of(CollageGrid::Dictionary).to receive(:random_words).
          and_return('television')

        described_class.flickr_search(searcher, term_without_result)
      end
    end
  end
end
