require 'spec_helper'

describe CollageGrid::Flickr::Photo do
  describe '#download_image' do
    context 'given result', vcr: {cassette_name: 'photo/ruby_photo'} do
      subject { described_class.new(result).download_image }
      let(:result) { CollageGrid::Flickr::Searcher.new.search('ruby') }

      it{ is_expected.to eq("./tmp/6354504245_6afea83ee8_n.jpg") }
    end
  end
end
