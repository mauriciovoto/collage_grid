require 'spec_helper'

describe CollageGrid::Flickr::Searcher do
  describe '.new' do
    it 'calls flickr auth' do
      expect(CollageGrid).to receive(:flickr_auth)
      described_class.new
    end

    context 'args' do
      let(:search_args) { described_class.new.send(:search_args) }

      it 'mounts search args accuracy correctly' do
        expect(search_args[:accuracy]).to eq 1
      end

      it 'mounts search args page correctly' do
        expect(search_args[:page]).to eq 1
      end

      it 'mounts search args per page correctly' do
        expect(search_args[:per_page]).to eq 1
      end

      it 'mounts search args media correctly' do
        expect(search_args[:media]).to eq 'photos'
      end

      it 'mounts search args sort correctly' do
        expect(search_args[:sort]).to eq 'interestingness-desc'
      end
    end
  end

  describe '#search' do
    context 'given an existing search term',
      vcr: {cassette_name: 'searcher/ruby_search'} do
      let(:word) { "ruby" }
      let(:expected_response) do
        {
          "id"=>"6354504245",
          "owner"=>"7482465@N07",
          "secret"=>"6afea83ee8",
          "server"=>"6032",
          "farm"=>7,
          "title"=>"Ruby Red Sunrise",
          "ispublic"=>1,
          "isfriend"=>0,
          "isfamily"=>0
        }
      end
      subject { described_class.new.search(word) }

      it 'responds with expected content' do
        expect(subject[0].to_hash).to eq expected_response
      end
    end

    context 'given a bizarre search term',
      vcr: {cassette_name: 'searcher/no_results_search'} do
      let(:word) { "nnnlduwiiac" }
      subject { described_class.new.search(word) }

      it 'responds with empty array' do
        expect{subject}.to raise_error(CollageGrid::Errors::Flickr::NotFound)
      end
    end
  end
end
