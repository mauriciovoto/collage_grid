Ruby Command Line Collage Grid
============================================

## Dependencies

[ImageMagick](https://www.imagemagick.org/script/index.php) or [GraphicsMagick](http://www.graphicsmagick.org/) command-line tool has to be installed.

## Instructions

### Setup and Install

Firstly, copy the ./config/flickr.yml.example into ./config/flickr.yml and add
your Flickr API credentials on this file.

After having the dependencies installed(as mentioned on the Dependencies
section), you just need to run the *bundle install* command to get all gem
dependencies.

### Tests

To run the entire test suite:

```
bundle exec rspec .
```

The coverage report is generated at ./coverage folder.

### Running the app

You need to provide at least one arg when running the app command, the output
filename(the name of the collage image generated).
You can also provide 11 args(being 10 words and 1 output filename), but not more
than that. And please, keep in mind that the last arg will be the output
filename

Example:

```
bundle exec rake app:start man ball bird flag baby output_filename
```

The final result will be at: ./tmp/output_filename.jpg

